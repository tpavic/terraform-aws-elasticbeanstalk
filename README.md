# Elastic Beanstalk WebServer Environment with Terraform

 ![Elastic Beanstalk Deployment with Terraform](src/eb-tf.png)

This terraform configuration will create two Elastic Beanstalk WebServer environments with Amazon Linux 2 and Docker, ACM certificates and DNS validation for domain.
For these specific environments domain that will be used is *.tpavic.me, HTTP -> HTTPS redirection is included and finally it will create A records for domain entries which will point to Beanstalk environments. 

In this example we used default VPC and default Beanstalk S3 bucket.

Before deployment create **key pair** and use name of that key pair in variables file and **Route53 Public Hosted Zone** for your domain.
