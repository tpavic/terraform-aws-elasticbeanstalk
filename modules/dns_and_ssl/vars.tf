variable "domain_name" {
  type = string
}

variable "domain_name_record" {
  type = string
}
variable "cname" {
  type = string
}

variable "zone" {
  type = string
}

