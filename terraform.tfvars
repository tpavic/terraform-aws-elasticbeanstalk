region = "us-east-1"

domain_name = "tpavic.me"

domain_name_stage = "stage.tpavic.me"

domain_name_prod  = "prod.tpavic.me"

app_tags = ""

app_tags_prod = "Prod"

app_tags_stage = "Stage"

application_name = "App"

application_name_stage = "Stage-App"

application_name_prod = "Prod-App"

vpc_id = "vpc-01e64839780434233"

ec2_subnets = "subnet-061126e92d7d7004f"

elb_subnets = ["subnet-061126e92d7d7004f","subnet-06c8bdec3d36852b2"]

instance_type = "t2.micro"

disk_size = "20"

keypair = "aws-key"

sshrestrict = "93.138.176.245/32"

alarm_sns_topic = "arn:aws:sns:us-east-1:791212775301:Default_CloudWatch_Alarms_Topic"

EbTags = "elastic-beanstalk"
