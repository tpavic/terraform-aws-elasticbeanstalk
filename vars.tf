variable "region" {}

variable "domain_name" {}

variable "domain_name_prod" {}

variable "domain_name_stage" {}

variable "app_tags" {}

variable "app_tags_prod" {}

variable "app_tags_stage" {}

variable "application_name" {}

variable "application_name_stage" {}

variable "application_name_prod" {}

variable "vpc_id" {}

variable "ec2_subnets" {}

variable "elb_subnets" {}

variable "instance_type" {}

variable "disk_size" {}

variable "keypair" {}

variable "sshrestrict" {}

variable "alarm_sns_topic" {}

variable "EbTags" {}
