provider "aws" {
  region  = var.region
}

module "stage-dns" {

  source = "./modules/dns_and_ssl/"
  
  domain_name                       = var.domain_name
  domain_name_record                = var.domain_name_stage
  cname                             = module.eb-stage.cname
  zone                              = module.eb-stage.zone
}

module "prod-dns" {

  source = "./modules/dns_and_ssl/"
  
  domain_name                       = var.domain_name
  domain_name_record                = var.domain_name_prod
  cname                             = module.eb-prod.cname
  zone                              = module.eb-prod.zone
}
module "eb-stage" {

  source = "./modules/beanstalk/"
  
  
  app_tags                          = var.app_tags_stage
  application_name                  = var.application_name_stage
  vpc_id                            = var.vpc_id
  ec2_subnets                       = var.ec2_subnets
  elb_subnets                       = var.elb_subnets
  instance_type                     = var.instance_type
  disk_size                         = var.disk_size
  keypair                           = var.keypair
  sshrestrict                       = var.sshrestrict
  certificate                       = module.stage-dns.certificate
}
module "eb-prod" {

  source = "./modules/beanstalk/"
  
  
  app_tags                          = var.app_tags_prod
  application_name                  = var.application_name_prod
  vpc_id                            = var.vpc_id
  ec2_subnets                       = var.ec2_subnets
  elb_subnets                       = var.elb_subnets
  instance_type                     = var.instance_type
  disk_size                         = var.disk_size
  keypair                           = var.keypair
  sshrestrict                       = var.sshrestrict
  certificate                       = module.prod-dns.certificate
}

